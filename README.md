

# Fork:
https://github.com/iamcanadian2222/MarantzIP


# For help:
 ```
./Commander.py -h
```

## Get functions
### Get help
 ```
./Commander.py get -h
 ```
### Sample
 ```
usage: Commander.py get [-h] {volumeL,powerState,source} ...

positional arguments:
  {volumeL,powerState,source}
                        Get subcommand
    volumeL             Get volume level
    powerState          Get power state
    source              Get source

optional arguments:
  -h, --help            show this help message and exit
```

## Set functions
### Set help
 ```
./Commander.py set -h
```
### Sample
```
usage: Commander.py set [-h] {powerState,volumeL,source} ...

positional arguments:
  {powerState,volumeL,source}
                        Set subcommand
    powerState          Set power State
    volumeL             Set volume level
    source              Set source.

optional arguments:
  -h, --help            show this help message and exit
```
