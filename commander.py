#!/usr/bin/python
import argparse
import time
import Marantz
import json
import requests


if __name__ == '__main__':

    # Init var
    sources = ["TUNER", "DVD", "BD", "TV", "SAT", "SAT/CBL", "MPLAY", "GAME", "AUX1", "NET", "PANDORA", "SIRIUSXM",
        "LASTFM", "FLICKR", "FAVORITES", "IRADIO", "SERVER", "USB/IPOD", "USB", "IPD", "IRP", "FVP"]

    # Init parser
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='Main commands', dest='command')

    #########################################################################################################
    # Get function
    #########################################################################################################
    get_parser = subparsers.add_parser('get', help='Get commands')
    get_subparsers = get_parser.add_subparsers(help='Get subcommand', dest='subcommand')

    # Device get volumeLevel -option volumeLevel
    get_volumeL = get_subparsers.add_parser('volume', help='Get volume level')
    get_volumeL.add_argument("address", help="IP device (Ex: 192.168.1.105)")

    # Device get power state -option powerState
    get_powerState = get_subparsers.add_parser('powerState', help='Get power state')
    get_powerState.add_argument("address", help="IP device (Ex: 192.168.1.105)")

    # Device get power state -option powerState
    get_source = get_subparsers.add_parser('source', help='Get source')
    get_source.add_argument("address", help="IP device (Ex: 192.168.1.105)")


    #########################################################################################################
    # Set functions
    #########################################################################################################
    set_parser = subparsers.add_parser('set', help='set commands')
    set_subparsers = set_parser.add_subparsers(help='Set subcommand', dest='subcommand')

    # set powerState
    set_powerState = set_subparsers.add_parser('powerState', help='Set power State')
    set_powerState.add_argument("address", help="IP device (Ex: 192.168.1.105)")
    set_powerState.add_argument("status", help="Set ON or OFF")

    # set volume level
    set_volumeL = set_subparsers.add_parser('volume', help='Set volume level')
    set_volumeL.add_argument("address", help="IP device (Ex: 192.168.1.105)")
    set_volumeL.add_argument("level", help="Volume level between 0-70")

    # set source
    set_source = set_subparsers.add_parser('source', help='Set source.')
    set_source.add_argument("address", help="IP device (Ex: 192.168.1.105)")
    set_source.add_argument("source", help='List:'.join(sources))

    #########################################################################################################
    # Conn args
    #########################################################################################################
    args = parser.parse_args()
    command = args.command
    subcommand = args.subcommand

    #########################################################################################################
    ####### Calling functions
    #########################################################################################################
    try:
        if command == "get":
            if subcommand == "volume":
                # print "[DEBUG] - COMMAND "+subcommand+" - Address " + (args.address)
                # Init conn
                avr = Marantz.IP(args.address)
                avr.connect()
                ## Get volume and check result
                dict1 = avr.get_volume()
                str1 = str(dict1)
                dict2 = eval(str1)
                if dict1==dict2:
                    # print "[DEBUG] - COMMAND "+subcommand+" - result: " + str1.replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(":",1)[1]
                    print str1.replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(":",1)[1]
                else:
                    print "[ERROR] - COMMAND "+subcommand+" - result fail"

            elif subcommand == "powerState":
                # print "[DEBUG] - COMMAND "+subcommand+" - Address " + (args.address)
                # Init conn
                avr = Marantz.IP(args.address)
                avr.connect()
                ## Get volume and check result
                dict1 = avr.get_power()
                str1 = str(dict1)
                dict2 = eval(str1)
                #print dict1
                if dict1==dict2:
                    print str1.replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(":",1)[1]
                    # print "[DEBUG] - COMMAND "+subcommand+" - result: " + str1.replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(":",1)[1]
                else:
                    print "[ERROR] - COMMAND "+subcommand+" - result fail"

            elif subcommand == "source":
                # print "[DEBUG] - COMMAND "+subcommand+" - Address " + (args.address)
                # Init conn
                avr = Marantz.IP(args.address)
                avr.connect()
                ## Get volume and check result
                dict1 = avr.get_source()
                str1 = str(dict1)
                dict2 = eval(str1)
                if dict1==dict2:
                    # print "[DEBUG] - COMMAND "+subcommand+" - result: " + str1.replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(":",1)[1]
                    print str1.replace("{", "").replace("}", "").replace("'", "").replace(" ", "").split(":",1)[1]
                else:
                    print "[ERROR] - COMMAND "+subcommand+" - result fail"


        #########################################################################################################
        ####### Calling Set functions
        #########################################################################################################
        if command == "set":
            if subcommand == "volume":
                try:
                    # Contert volume value
                    absouleValue = int(args.level)
                    finalValue = 80 - absouleValue
                    finalStringValue = str(finalValue)

                    # Post command & check response
                    r = requests.get('http://' + args.address + ':8080/goform/formiPhoneAppVolume.xml?1+-' + finalStringValue)
                    if r.status_code == 200:
                        print "[DEBUG] - New volume value is " + finalStringValue
                    else:
                        print "[ERROR] - Cannot change volume value"

                except Exception as e:
                    print "[ERROR] - Failed to Convert volume value: " + str(e)

            elif subcommand == "powerState":
                powerCommand = ""
                if args.status == "ON":
                    powerCommand = "PowerOn"
                elif args.status == "OFF":
                    powerCommand = "PowerStandby"
                else:
                    print "[ERROR] - Please check status value. " + args.status + " is not a valid value"
                    exit()
                # Post command & check response
                r = requests.get('http://' + args.address + ':8080/goform/formiPhoneAppPower.xml?1+' + powerCommand)
                if r.status_code == 200:
                    print "[DEBUG] - New status is " + powerCommand
                else:
                    print "[ERROR] - Cannot change status"

            elif subcommand == "source":
                # Post command & check response
                r = requests.get('http://' + args.address + ':8080/goform/formiPhoneAppDirect.xml?SI'+ args.source)
                if r.status_code == 200:
                    print "[DEBUG] - New source is " + args.source
                else:
                    print "[ERROR] - Cannot change source with " + args.source

    except Exception as e:
        print "[CRITICAL-ERROR] - Calling functions. Exception: " + str(e)
        #raise
